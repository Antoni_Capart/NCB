<?php

// Variable qui contient la réponse pour le serveur 200 ou 400
$reponse_data = "0";

// Variable qui contient l'URL pour l'envoie des données final
$url = "http://nightcode-phobos.cleverapps.io/input/science-datas/add";



// Vérification du type de contenu qui doit être de type json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';



// Récupération et décodage du fichier reçu
$content = trim(file_get_contents("php://input"));
if ($contentType == "application/json")
	$decoded = json_decode($content, true);



// Vérification de la forme du fichier json
if (verification_body_json($decoded) == true)
	$reponse_data = 200;
else
	$reponse_data = 400;



//if( $_POST["payload"] && $_POST["uuid"]){
	//$payload=$_POST['payload'];
	$payload=[[0,0,1,1,1,1,1,1,1,0,1,0,0,0,1,1,1,0,1,1,1,1,0,1,0,0,1,1,1,1,1,0,0,1,0,0,1,1,1,0,0,1,0,0,1,1,1,1,0,1,1,0,1,1,0,0,1,1,0,1,0,1,0,1,1,1,1,1,0,1,0,0,0,1,1,0,1,1,0,1,0,1,0,0,0,0,0,1,1,1,0,1,0,1,1,1,0,1,0,1],[1,0,1,0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,1,0,1,1,1,0,1,0,0,1,1,0,1,1,1,0,1,1,1,0,1,0,0,1,0,0,1,1,1,0,1,0,1,1,1,1,1,0,0,0,1,1,1,0,1,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,1,1,0,1,0,0,0,1,1,1,1,1,1,0,0,0,1,0,1,0,1,1],[1,1,1,1,1,0,1,0,1,1,0,1,0,1,1,1,0,1,1,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,0,1,0,1,1,0,0,1,0,1,1,1,1,1,0,0,0,0,1,0,0,0,0,1,1,0,0,1,0,0,0,0,0,1,1,0,1,1,0,1,0,0,0,0,1,0,0,1,0,0,1,1,1,1,1,1,0,0,0,1,1,1,1,1,0,1],[0,0,0,1,1,0,0,1,0,0,1,0,1,0,1,0,0,0,0,0,0,1,0,0,0,1,1,1,0,1,0,1,0,0,0,1,1,1,1,0,0,1,1,1,0,1,1,1,1,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,1,1,0,0,1,1,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,1,0,0,1,1,1,0,0],[0,1,0,1,1,0,1,0,1,0,0,0,0,0,1,0,0,1,1,1,1,0,0,0,0,1,0,0,0,0,1,0,0,0,1,1,1,0,1,1,1,1,0,0,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1,0,1,1,1,0,1,0,0,0,0,1,0,0,1,1,1,1,1,1,0,1,1,0,0,1,0,1,0]];
	$matrice= explode(",", $payload);
	//$matrice=$payload;
		
//	}


// echo "\n";
// print_r($matrice);

$encoded =base64_encode($matrice); 

// print_r($encoded);



// Envoie de la réponse au serveur
envoi_reponse_server($reponse_data);

// Envoie des données
envoi_data($decoded, $url);





// Fonction qui vérifie si le fichier json décodé donnée en paramètre contient les bonnes données ou non
function verification_body_json($json_decoded)
{
	$verif = true;
	
	if (isset($json_decoded["uuid"]) == false)
		$verif = false;
	
	if (isset($json_decoded["payload"]) == false)
		$verif = false;
	
	if (count($json_decoded['payload']) <= 1)
		$verif = false;
	
	return $verif;
}

// Fonction qui envoie la réponse pour le serveur qui à envoyer les données
function envoi_reponse_server($reponse)
{
	$rep = array('status' => $reponse);
	echo json_encode($rep);
	header(".", true, $reponse);
}

// Fonction qui envoie les données convertie au serveur cible
function envoi_data($data, $urlSend)
{
	
}

?>

