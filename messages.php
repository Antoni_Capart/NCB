<?php
include("envoi_data.php");
 
$PostIsOk=true;
$returnData="0";
 //Make sure that it is a POST request.
if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
    $PostIsOk=false;
}
 
//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if(strcasecmp($contentType, 'application/json') != 0){
    $PostIsOk=false;
}
 
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));
 
//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);

//If json_decode failed, the JSON is invalid.
if(!is_array($decoded)){
    $PostIsOk=false;
}
else{
	if(isset($decoded["type"]) ){
		if(isset($decoded["payload"]) ){
			if(isset($decoded["payload"]["messages"]) && count($decoded["payload"]["messages"])>1){
		        foreach($decoded["payload"]["messages"] as $msg){
		            if(!(isset($msg["uuid"]) && ( isset($msg["msg"]) || isset($msg["text"])  || isset($msg["content"])) )){
		                $PostIsOk=false;
		            }
		        }
	    	}
    	}
    	else if (isset($decoded["messages"]) && count($decoded["messages"])>1){
    		foreach($decoded["messages"] as $msg){
	            if(!(isset($msg["uuid"]) && ( isset($msg["msg"]) || isset($msg["text"])  || isset($msg["content"])) )){
	                $PostIsOk=false;
	            }
	        }
    	}
    }
    else if(isset($decoded["payload"]) && isset($decoded["payload"]["inputs"]) && count($decoded["payload"]["inputs"])>1){
        foreach($decoded["payload"]["inputs"] as $msg){
            if(!(isset($msg["uuid"]) && isset($msg["msg"]) /*|| isset($msg["text"])  || isset($msg["content"])*/ )) {
                $PostIsOk=false;
            }
        }

    }
}

 if (count($decoded["payload"]["inputs"])==0 && count($decoded["payload"]["messages"])==0 && count($decoded["messages"])==0) {
        	$PostIsOk=false;
 }

if($PostIsOk){
    $returnData= array('status' => "200");
    header(".",TRUE,200);
    if(isset($decoded["type"])){
        $mes="messages";
        if($decoded["type"]=="for-mr-clerentin"){
            $cont="content";
        }
        else{
            $cont="text";
        }
    }
    else{
        $mes="inputs";
        $cont="msg";
    }

    foreach($decoded["payload"][$mes] as $msg){
        $data=array("external_id"=>$msg["uuid"],"content"=>$msg[$cont]);
        function_envoi_data("input/messages",$data);
    }

}
else
{
    $returnData= array('status' => "400");
    header(".",TRUE,400);
}

echo json_encode($returnData);

?>