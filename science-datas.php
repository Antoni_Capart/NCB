<?php

// Variable qui contient la réponse pour le serveur 200 ou 400
$reponse_data = "0";

// Variable qui contient l'URL pour l'envoie des données final
$url = "http://nightcode-phobos.cleverapps.io/input/science-datas/add";

// Variable qui contient l'API TOKEN
$API_TOKEN = "898929c5c1121a2b71d3668e26fea165e77b7cd9395f06481a04e8bb677af2bb";



// Vérification du type de contenu qui doit être de type json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';



// Récupération et décodage du fichier reçu
$content = trim(file_get_contents("php://input"));
if ($contentType == "application/json")
	$decoded = json_decode($content, true);
else if ($contentType == "text/plain")
	$decoded = convertion_text_to_array($content);



// Vérification de la forme du fichier json
if (verification_body_json($decoded) == true)
	$reponse_data = 200;
else
	$reponse_data = 400;



// Conversion des données reçu
conversion_data($decoded);



// Envoie de la réponse au serveur
envoi_reponse_server($reponse_data);

// Envoie des données
envoi_data($decoded, $url, $API_TOKEN);





// Fonction qui convertie du text/plain en tableau
function convertion_text_to_array($data)
{
	$tabTemp = explode("\\n", $data);
	$tab = str_replace("\\", "", $tabTemp);
	$tab = str_replace("\"", "", $tab);
	
	$res = [
		"uuid" => $tab[0],
		"datas" => array(),
	];

	for ($i = 1; $i < count($tab); $i++)
	{
		$ligne = explode(',', $tab[$i]);
		
		$res["datas"][$i - 1] = [
			"type" => $ligne[0],
			"unit" => $ligne[1],
			"value" => $ligne[2],
		];
	}
	
	return $res;
}

// Fonction qui convertie les valeurs contenu dans le fichier json décodé passé en paramètre
function conversion_data($json_decoded)
{
	foreach($json_decoded['datas'] as $datas)
	{
		if ($datas['unit'] == "celsius-degrees")
		{
			$datas['unit'] = "fahrenheit-degrees";
			$datas['value'] = $datas['value'] * 1.8 + 32;
		}
		
		if ($datas['unit'] == "milli-pirate-ninjas")
		{
			$datas['unit'] = "kilo-joules";
			$datas['value'] = $datas['value'] * 3600;
		}
		
		if ($datas['unit'] == "pounds")
		{
			$datas['unit'] = "kilograms";
			$datas['value'] = $datas['value'] * 0.45359;
		}
		
		if ($datas['unit'] == "gallons")
		{
			$datas['unit'] = "liters";
			$datas['value'] = $datas['value'] * 3.78541178;
		}
	}
}

// Fonction qui vérifie si le fichier json décodé donnée en paramètre contient les bonnes données ou non
function verification_body_json($json_decoded)
{
	$verif = true;
	
	if (isset($json_decoded["uuid"]) == false)
		$verif = false;
	
	if (isset($json_decoded["datas"]) == false)
		$verif = false;
	
	if (count($json_decoded['datas']) <= 1)
		$verif = false;
	
	foreach($json_decoded['datas'] as $datas)
	{
		if (isset($datas["type"]) == false)
			$verif = false;
		
		if (isset($datas["unit"]) == false)
			$verif = false;
		
		if (isset($datas["value"]) == false)
			$verif = false;
	}
	
	return $verif;
}

// Fonction qui envoie la réponse pour le serveur qui à envoyer les données
function envoi_reponse_server($reponse)
{
	$rep = array('status' => $reponse);
	echo json_encode($rep);
	header(".", true, $reponse);
}

// Fonction qui envoie les données convertie au serveur cible
function envoi_data($data, $urlSend, $apiToken)
{
	/*
	use GuzzleHttp\Client;
	
	$headers = [
		'Accept' => 'application/json',
		'Content-Type' => 'application/json',
		'X-API-Key' => '898929c5c1121a2b71d3668e26fea165e77b7cd9395f06481a04e8bb677af2bb'
	];
	
	$body = "Ghost !";
	
	$request = new Request('POST', $urlSend, $headers, $body);
	
	$response = $client->post('http://httpbin.org/post');
	
	echo 'Cool' . $response;
	*/
}

?>

